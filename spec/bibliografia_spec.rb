require "spec_helper"

describe Referencia do
	before :each do
    	@referencia = Referencia.new(["Dave Thomas"],"Programming Ruby 1.9 & 2.0: The Programatic Programers Guide","July 7, 2013")
  	end

	it "Comprobar que hay por lo menos un autor" do
		@referencia.m_autores.length.should_not be 0
	end

	it "Debe Existir un Título." do
		expect(@referencia.m_titulo).to eq("Programming Ruby 1.9 & 2.0: The Programatic Programers Guide")
	end

	it "Debe existir una fecha de publicación." do
		expect(@referencia.m_fechaPublicacion).not_to eq("")
	end
	
	it "Comprobar que una Referencia hereda de Object" do
		expect(@referencia.class.superclass).equal?(Object)
	end
	
	it "Comprobar que una Referencia hereda de BasicObject" do
		expect(@referencia.class.superclass.superclass).equal?(BasicObject)
	end
end

describe Revista do
	before :each do
    	@revista = Revista.new(["Javier abad"],"Pokemon vuelve a salir 20 años después","July 21, 2015","Hobby Consolas","Volumen I","270","159")
  	end

	it "Comprobar que hay por lo menos un autor" do
		@revista.m_autores.length.should_not be 0
	end

	it "Debe Existir un Título." do
		expect(@revista.m_titulo).to eq("Pokemon vuelve a salir 20 años después")
	end

	it "Debe existir una fecha de publicación." do
		expect(@revista.m_fechaPublicacion).not_to eq("")
	end
	
	it "Debe existir un nombre de revista" do
		expect(@revista.m_nombreRevista).not_to eq("")
	end
	
	it "Debe existir un volumen" do
		expect(@revista.m_volumen).not_to eq("")
	end
	
	it "Debe existir un numero" do
		expect(@revista.m_numero).not_to eq("")
	end
	
	it "Debe existir un numero de páginas" do
		expect(@revista.m_paginas).not_to eq("")
	end
	
	it "Comprobar que una revista hereda de Referencia" do
		expect(@revista.class.superclass).equal?(Referencia)
	end
end

describe Libro do
	before :each do
    	@libro = Libro.new(["John Verdon"],"Controlaré tus sueños","July 7, 2013","S/C de tenerife","España","Espasa","52147896")
  	end

	it "Comprobar que hay por lo menos un autor" do
		@libro.m_autores.length.should_not be 0
	end

	it "Debe Existir un Título." do
		expect(@libro.m_titulo).to eq("Controlaré tus sueños")
	end

	it "Debe existir una fecha de publicación." do
		expect(@libro.m_fechaPublicacion).not_to eq("")
	end
	
	it "Debe existir una ciudad" do
		expect(@libro.m_ciudad).not_to eq("")
	end
	
	it "Debe existir un país" do
		expect(@libro.m_pais).not_to eq("")
	end
	
	it "Debe existir una editorial" do
		expect(@libro.m_editorial).not_to eq("")
	end
	
	it "Debe existir un isbn" do
		expect(@libro.m_isbn).not_to eq("")
	end
	
	it "Comprobar que un libro hereda de Referencia" do
		expect(@libro.class.superclass).equal?(Referencia)
	end
end

describe Periodico do
	before :each do
    	@periodico = Periodico.new(["Yamcha,Chaoz,Ten shi han"],"Los supervivientes","July 21, 2015","La noche","30")
  	end

	it "Comprobar que hay por lo menos un autor" do
		@periodico.m_autores.length.should_not be 0
	end

	it "Debe Existir un Título." do
		expect(@periodico.m_titulo).to eq("Los supervivientes")
	end

	it "Debe existir una fecha de publicación." do
		expect(@periodico.m_fechaPublicacion).not_to eq("")
	end
	
	it "Debe existir un nombre de periodico" do
		expect(@periodico.m_nombrePeriodico).not_to eq("")
	end
	
	it "Debe existir un numero de páginas" do
		expect(@periodico.m_paginas).not_to eq("")
	end
	
	it "Comprobar que un Periodico hereda de Referencia" do
		expect(@periodico.class.superclass).equal?(Referencia)
	end
end

describe DocumentoElectronico do
	before :each do
    	@documentoElectronico = DocumentoElectronico.new(["Yamcha,Chaoz,Ten shi han"],"Guia docente 2015-2016","July 21, 2015","https://www.casadellibro.com")
  	end

	it "Comprobar que hay por lo menos un autor" do
		@documentoElectronico.m_autores.length.should_not be 0
	end

	it "Debe Existir un Título." do
		expect(@documentoElectronico.m_titulo).to eq("Guia docente 2015-2016")
	end

	it "Debe existir una fecha de publicación." do
		expect(@documentoElectronico.m_fechaPublicacion).not_to eq("")
	end
	
	it "Debe existir una url" do
		expect(@documentoElectronico.m_url).to eq("https://www.casadellibro.com")
	end
	
	it "Comprobar que un Documento Electronico hereda de Referencia" do
		expect(@documentoElectronico.class.superclass).equal?(Referencia)
	end
end

describe Node do
    before:all do
        @referencia = Referencia.new(["Dave Thomas"],"Programming Ruby 1.9 & 2.0: The Programatic Programers Guide","July 7, 2013")
        @node = Node.new(@referencia, nil, nil)
    end
	  it "#Debe existir un Nodo de la lista con sus datos y su siguiente" do
	     expect(@node.value).not_to eq(nil)
	  end
end

describe Lista do
	before:each do
	    @libro = Libro.new(["John Verdon"],"Controlaré tus sueños","July 7, 2013","S/C de tenerife","España","Espasa","52147896")
	    @revista = Revista.new(["Javier abad"],"Pokemon vuelve a salir 20 años después","July 21, 2015","Hobby Consolas","Volumen I","270","159")
	    @periodico = Periodico.new(["Yamcha,Chaoz,Ten shi han"],"Los supervivientes","July 21, 2015","La noche","30")
	    @documento = DocumentoElectronico.new(["Yamcha,Chaoz,Ten shi han"],"Guia docente 2015-2016","July 21, 2015","https://www.casadellibro.com")
		@lista = Lista.new()
		@lista.insert(@libro)
		@lista.insert(@revista)
		@lista.insert(@periodico)
		@lista.insert(@documento)
	end
	
	it "#Existe una lista vacia" do
		expect(Lista.new().empty?).to eq(true)
	end
	
	it "#Se puede insertar un elemento" do
		@lista.insert(@documento)
	end
	
	it "#Se extrae el primer elemento de la lista" do
		@lista.extractFirstElement
	end

	it "#Se pueden insertar varios elementos" do
		@lista.insert(@revista,@libro)
	end
	
	it "#Debe existir una Lista con su cabeza" do
	    expect(@lista.m_fin.value).to eq(@documento)
	end
end
