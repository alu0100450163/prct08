class Periodico<Referencia
    attr_reader :m_nombrePeriodico , :m_paginas
    #Constructor con todos los parámetros
    def initialize(a_autores,a_titulo,a_fechaPublicacion,a_nombrePeriodico,a_paginas)
       super(a_autores,a_titulo,a_fechaPublicacion)
       @m_nombrePeriodico = a_nombrePeriodico
       @m_paginas = a_paginas
    end
end