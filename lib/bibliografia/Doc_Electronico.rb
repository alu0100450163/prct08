class DocumentoElectronico<Referencia
    attr_reader :m_url
    #Constructor con todos los parámetros
    def initialize(a_autores,a_titulo,a_fechaPublicacion,a_url)
       super(a_autores,a_titulo,a_fechaPublicacion)
       @m_url = a_url
    end
end