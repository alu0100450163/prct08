class Libro<Referencia
    attr_reader :m_ciudad , :m_pais , :m_editorial , :m_isbn
    #Constructor con todos los parámetros
    def initialize(a_autores,a_titulo,a_fechaPublicacion,a_ciudad,a_pais,a_editorial,a_isbn)
       super(a_autores,a_titulo,a_fechaPublicacion)
       @m_ciudad = a_ciudad
       @m_pais = a_pais
       @m_editorial = a_editorial
       @m_isbn = a_isbn
    end
end