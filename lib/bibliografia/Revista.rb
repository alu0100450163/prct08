class Revista<Referencia
    attr_reader :m_nombreRevista , :m_volumen , :m_numero , :m_paginas
    #Constructor con todos los parámetros
    def initialize(a_autores,a_titulo,a_fechaPublicacion,a_nombreRevista,a_volumen,a_numero,a_paginas)
       super(a_autores,a_titulo,a_fechaPublicacion)
       @m_nombreRevista = a_nombreRevista
       @m_volumen = a_volumen
       @m_numero = a_numero
       @m_paginas = a_paginas
    end
end