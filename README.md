# Referencia
# Lenguajes y Paradigmas de Programación
## Práctica nº 8
## Manuel Alejandro Bacallado López.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'bibliografia'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install bibliografia
